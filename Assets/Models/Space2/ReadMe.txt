Thank you for checking out my free models.
They were made using my 3ds max script "Pattern Generator" which generates
cool looking patterns using one simple click! 
Check it out at : www.code-artists.de/pattern.html

Thank you and have fun with the models!